# BlueTooth Low Energy Standard Protocol
This App is used to implement Bluetooth standard protocol that use InMethod Android BlueTooth FrameWork.    
https://bitbucket.org/inmethod/bluetoothframework    

## System Requirement 
* Android 4.3 or above
* Android wear 5.0 or above

## Develop Environment
* Android Studio 2.2 or above

## BlueTooth Heart Rate Measurement ( 0x2A37 )
https://developer.bluetooth.org/gatt/characteristics/Pages/CharacteristicViewer.aspx?u=org.bluetooth.characteristic.heart_rate_measurement.xml

## BlueTooth Battery Level ( 0x2A19)
https://developer.bluetooth.org/gatt/characteristics/Pages/CharacteristicViewer.aspx?u=org.bluetooth.characteristic.battery_level.xml

## HLMT java doc
HTML document can be found at project "BleStdProtocol/docs" 

## Example Project
https://bitbucket.org/inmethod/simpleblehrbandmonitor    Heart Rate    
https://bitbucket.org/inmethod/blehrbandmonitor   Heart Rate & Battery Level    