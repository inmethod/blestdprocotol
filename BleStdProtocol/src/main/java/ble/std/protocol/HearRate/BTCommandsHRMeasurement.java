package ble.std.protocol.HearRate;

import android.os.Bundle;
import android.os.Message;
import android.util.Log;

import inmethod.android.bt.GlobalSetting;
import inmethod.android.bt.command.BTCommand;
import inmethod.android.bt.command.BTCommands;
import inmethod.android.bt.command.BTNotificationCommand;

/**
 *
BlueTooth Heart Rate Measurement ( 0x2A37 )
https://developer.bluetooth.org/gatt/characteristics/Pages/CharacteristicViewer.aspx?u=org.bluetooth.characteristic.heart_rate_measurement.xml
<pre> <font color='red'>
        BTCommands aBTCommands = new BTCommandsHRMeasurement(iDefaultAge);
            aBTCommands.setCallBackHandler(
                    new BTCommandsHandler() {
                        {@literal @}Override
                        public void BTCommandsHeartRateMeasurementSuccess(HeartRateValue aHeartRateValue,BTInfo aInfo){
                            if (aHeartRateValue == null) return;
                            else iCurrentHR = aHeartRateValue.getHeartRate();
                            Toast.makeText(activity, "Heart Rate = " + iCurrentHR, Toast.LENGTH_SHORT).show();
                            Log.d(TAG, "Current HeartRate=" + iCurrentHR);
                        }

                        {@literal @}Override
                        public void BTCommandsHeartRateMeasurementTimeout(BTInfo aInfo){
                            Toast.makeText(activity, "command Timeout", Toast.LENGTH_SHORT).show();
                            Log.d(TAG, "command Timeout");
                            aBlueToothDeviceConnection.stop();
                        }
                    }
            );
            aBlueToothDeviceConnection.sendBTCommands(aBTCommands);
</font></pre>
 */
public class BTCommandsHRMeasurement extends BTCommands{
    protected static final String TAG = "InMethod-BleStdProtocol";
    public static final String HEART_RATE_MEASUREMENT_UUID = "00002a37-0000-1000-8000-00805f9b34fb";
    public static final String CALLBACK_HEART_RATE_MEASUREMENT_SUCCESS = "CALLBACK_HEART_RATE_MEASUREMENT_SUCCESS";
    public static final String CALLBACK_HEART_RATE_MEASUREMENT_TIMEOUT = "CALLBACK_HEART_RATE_MEASUREMENT_TIMEOUT";
    private int iCounter = 0;
    private int iCounterSize = 2;
    private byte[] byteCounterArray = new byte[2];
    private boolean bIsHRFormatDouble = false;
    private int iAge = 25;

    private BTCommandsHRMeasurement(){

    }


    @Override
    public void handleTimeout() throws Exception {
        Message aMessage = getCallBackHandler().obtainMessage(1, CALLBACK_HEART_RATE_MEASUREMENT_TIMEOUT);
        Bundle aBundle = new Bundle();
        aBundle.putParcelable(GlobalSetting.BUNDLE_KEY_BLUETOOTH_INFO, getCurrentConnection().getBTInfo());
        aMessage.setData(aBundle);
        getCallBackHandler().sendMessage(aMessage);
        setFinished(true);
    }

    public BTCommandsHRMeasurement(int iAge){
        this.iAge = iAge;
        BTCommand aCmd = new BTNotificationCommand(HEART_RATE_MEASUREMENT_UUID);
        addCommand(aCmd);
        setTimeout(10000);
    }

    /**
     *
     * @param rawData data response from bluetooth device
     *  @param objUUID  String UUID
     * @return  HeartRateValue  or  null  not ready or HR  unreasonable
     */
    @Override
    public void getData(byte rawData,Object objUUID) throws Exception {
        if (objUUID.toString().equalsIgnoreCase(HEART_RATE_MEASUREMENT_UUID)) {
            HeartRateValue aReturn = null;
            iCounter++;
            if (iCounter > iCounterSize) iCounter = 1;
            if (iCounter == 2) byteCounterArray[0] = rawData;
            if (iCounter == 3 && bIsHRFormatDouble) byteCounterArray[1] = rawData;

            if (iCounter == 1) {
                if ((rawData & 0x01) == 1) {
                    iCounterSize = 3;
                    bIsHRFormatDouble = true;
                } else {
                    iCounterSize = 2;
                    bIsHRFormatDouble = false;
                }
                if ((rawData & 0x08) == 8) {
                    iCounterSize = iCounterSize + 2;
                }
                if ((rawData & 0x10) == 16) {
                    iCounterSize = iCounterSize + 2;
                }
                return;
            }

            if (iCounter == 2 && !bIsHRFormatDouble) {
                aReturn = new HeartRateValue(iAge);
                aReturn.setHeartRate((byteCounterArray[0] & 0xff));
            } else if (iCounter == 3 && bIsHRFormatDouble) {
                aReturn = new HeartRateValue(iAge);
                aReturn.setHeartRate((byteCounterArray[0] & 0xff) + (byteCounterArray[1] & 0xff) * 256);
            }

            if (objUUID.toString().equalsIgnoreCase(HEART_RATE_MEASUREMENT_UUID) && aReturn != null) {
                Message aMessage = getCallBackHandler().obtainMessage(1, CALLBACK_HEART_RATE_MEASUREMENT_SUCCESS);
                Bundle aBundle = new Bundle();

                aBundle.putParcelable(GlobalSetting.BUNDLE_KEY_DATA, aReturn);
                aBundle.putParcelable(GlobalSetting.BUNDLE_KEY_BLUETOOTH_INFO, getCurrentConnection().getBTInfo());
                aMessage.setData(aBundle);
                getCallBackHandler().sendMessage(aMessage);
            }
        }
    }
}
