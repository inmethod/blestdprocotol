package ble.std.protocol.HearRate;

import android.os.Parcel;
import android.os.Parcelable;


public class HeartRateValue  implements Parcelable {
    protected int iHR = 72;
    protected int iAge = 25;
    private HeartRateValue(){};
    public HeartRateValue(int iAge){
      this.iAge = iAge;
    }

    protected HeartRateValue(Parcel in) {
        iHR = in.readInt();
        iAge = in.readInt();
    }

    public static final Creator<HeartRateValue> CREATOR = new Creator<HeartRateValue>() {
        @Override
        public HeartRateValue createFromParcel(Parcel in) {
            return new HeartRateValue(in);
        }

        @Override
        public HeartRateValue[] newArray(int size) {
            return new HeartRateValue[size];
        }
    };



    public void setHeartRate(int iHR){
        this.iHR = iHR;
    }
    public int getHeartRate(){
        return iHR;
    }
    public void setAge(int iAge){this.iAge = iAge;}
    public int getiAge(){return iAge;}
    /**
     *
     * @return  n  will be  n  percentage Heart Rate Zone
     */
    public int getHeartRatePercentageZone(){
      return (iAge / (220-iAge))*100;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel arg0, int arg1) {
        arg0.writeInt(iHR);
        arg0.writeInt(iAge);

    }

    public void readFromParcel(Parcel in) {
        iHR = in.readInt();
        iAge = in.readInt();
    }
}