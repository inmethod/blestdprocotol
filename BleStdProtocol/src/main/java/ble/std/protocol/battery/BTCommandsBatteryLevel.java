package ble.std.protocol.battery;

import android.os.Bundle;
import android.os.Message;

import java.util.Arrays;

import inmethod.android.bt.GlobalSetting;
import inmethod.android.bt.command.BTCommand;
import inmethod.android.bt.command.BTCommands;
import inmethod.android.bt.command.BTReadCommand;

/**
 * This Command Support Standard  BLE Battery Service
 * https://developer.bluetooth.org/gatt/services/Pages/ServiceViewer.aspx?u=org.bluetooth.service.battery_service.xml


<pre> <font color='red'>
  BTCommands aBatteryLevel = new BTCommandsBatteryLevel();
  aBatteryLevel.setCallBackHandler(new BTCommandsHandler() {
   {@literal @}Override
    public void BTCommandsBatteryLevelSuccess(BTInfo aBTInfo, final int iBetteryLevel) {
      runOnUiThread(new Runnable() {
        public void run() {
          aTvBetteryStatus.setText("HeartRate and Time , Battery = "+iBetteryLevel);
        }
      });
    }
    {@literal @}Override
    public void BTCommandsBatteryLevelTimeout(BTInfo aBTInfo) {
      runOnUiThread(new Runnable() {
        public void run() {
          aTvBetteryStatus.setText("HeartRate and Time");
        }
      });
    }
  });
  aBlueToothDeviceConnection.sendBTCommands(aBatteryLevel);
 </font></pre>
 */
public class BTCommandsBatteryLevel extends BTCommands {

    public static final String BATTERY_LEVEL_UUID = "00002A19-0000-1000-8000-00805f9b34fb";
    public static final String CALLBACK_BATTERY_LEVEL_SUCCESS = "CALLBACK_BATTERY_LEVEL_SUCCESS";
    public static final String CALLBACK_BATTERY_LEVEL_TIMEOUT = "CALLBACK_BATTERY_LEVEL_TIMEOUT";

    public BTCommandsBatteryLevel() {
        BTCommand aReadBTCommand = new BTReadCommand(BATTERY_LEVEL_UUID);
        addCommand(aReadBTCommand);
        setTimeout(2000);
    }

    @Override
    public void getData(byte byteData, Object o) throws Exception {
        if( o.toString().equalsIgnoreCase(BATTERY_LEVEL_UUID)) {
            Message aMessage = getCallBackHandler().obtainMessage(1, CALLBACK_BATTERY_LEVEL_SUCCESS);
            Bundle aBundle = new Bundle();
            aBundle.putByte(GlobalSetting.BUNDLE_KEY_DATA, byteData);
            aBundle.putParcelable(GlobalSetting.BUNDLE_KEY_BLUETOOTH_INFO, getCurrentConnection().getBTInfo());
            aMessage.setData(aBundle);
            getCallBackHandler().sendMessage(aMessage);
            setFinished(true);
        }
    }

    @Override
    public void handleTimeout() throws Exception {
        Message aMessage = getCallBackHandler().obtainMessage(1, CALLBACK_BATTERY_LEVEL_TIMEOUT);
        Bundle aBundle = new Bundle();
        aBundle.putParcelable(GlobalSetting.BUNDLE_KEY_BLUETOOTH_INFO, getCurrentConnection().getBTInfo());
        aMessage.setData(aBundle);
        getCallBackHandler().sendMessage(aMessage);
        setFinished(true);
    }

}
