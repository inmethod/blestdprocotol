package ble.std.protocol.BTCommandsHandler;

import android.os.Bundle;
import android.os.Message;

import ble.std.protocol.HearRate.BTCommandsHRMeasurement;
import ble.std.protocol.HearRate.HeartRateValue;
import ble.std.protocol.battery.BTCommandsBatteryLevel;
import inmethod.android.bt.BTInfo;
import inmethod.android.bt.GlobalSetting;



/**
 * Created by william on 2016/7/14.
 */
public class BTCommandsHandler extends inmethod.android.bt.handler.CommandCallbackHandler {

    protected static final String TAG = "InMethod-BleStdProtocol";
    Bundle aBundle = null;
    BTInfo aInfo = null;
    HeartRateValue aHeartRateValue = null;
    @Override
    public void handleCommandResponsedMessage(Message msg) {
        aBundle = msg.getData();
        switch ( (String)msg.obj) {
            case BTCommandsBatteryLevel. CALLBACK_BATTERY_LEVEL_SUCCESS:

                byte byteData =  aBundle.getByte(GlobalSetting.BUNDLE_KEY_DATA);
                aInfo = aBundle.getParcelable(GlobalSetting.BUNDLE_KEY_BLUETOOTH_INFO);
                BTCommandsBatteryLevelSuccess(aInfo, (int)(byteData&0xff));
                break;
            case BTCommandsBatteryLevel. CALLBACK_BATTERY_LEVEL_TIMEOUT:
                aInfo = aBundle.getParcelable(GlobalSetting.BUNDLE_KEY_BLUETOOTH_INFO);
                BTCommandsBatteryLevelTimeout(aInfo);
                break;
            case BTCommandsHRMeasurement.CALLBACK_HEART_RATE_MEASUREMENT_SUCCESS:

                aHeartRateValue = aBundle.getParcelable(GlobalSetting.BUNDLE_KEY_DATA);
                aInfo = aBundle.getParcelable(GlobalSetting.BUNDLE_KEY_BLUETOOTH_INFO);
                BTCommandsHeartRateMeasurementSuccess(aHeartRateValue,aInfo);
                break;
            case BTCommandsHRMeasurement.CALLBACK_HEART_RATE_MEASUREMENT_TIMEOUT:

                aInfo = aBundle.getParcelable(GlobalSetting.BUNDLE_KEY_BLUETOOTH_INFO);
                BTCommandsHeartRateMeasurementTimeout(aInfo);
                break;
        }
    }

    /**
     *
     * @param aBTInfo
     * @param iBetteryLevel
     */
    public void BTCommandsBatteryLevelSuccess(BTInfo aBTInfo, int iBetteryLevel){}

    public void BTCommandsBatteryLevelTimeout(BTInfo aBTInfo){}

    public void BTCommandsHeartRateMeasurementSuccess(HeartRateValue aHeartRateValue,BTInfo aBTInfo){}

    public void BTCommandsHeartRateMeasurementTimeout(BTInfo aBTInfo){}
}


